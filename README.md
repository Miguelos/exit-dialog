exit-dialog
===========

Exit dialog using Ruby-GNOME2.

Based on [cb-exit](https://github.com/super-nathan/cb-exit).


[![endorse](http://api.coderwall.com/miguelos/endorsecount.png)](http://coderwall.com/miguelos)
